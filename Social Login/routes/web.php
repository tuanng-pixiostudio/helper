<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Social Login
Route::get('/auth/{provider}', 'UserSocialController@redirectToProvider')->name('');
Route::get('/auth/{provide}/callback', 'UserSocialController@handleProviderCallback')->name('');