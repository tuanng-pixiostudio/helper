**Social Login with Laravel**

Tested in Laravel 5.8

---

## Install Socialite

composer require laravel/socialite

1. Add into **config/app.php** 
```php
'providers' => [
   // Other service providers...

   Laravel\Socialite\SocialiteServiceProvider::class,
],
```
2. Add Alias 
```php'Socialite' => Laravel\Socialite\Facades\Socialite::class,```
3. Create **App Facebook** https://developers.facebook.com/
4. Add into **config/services.php** 
```php
'facebook' => [
    'client_id' => env('FACEBOOK_APP_ID'),
    'client_secret' => env('FACEBOOK_APP_SECRET'),
    'redirect' => env('FACEBOOK_APP_CALLBACK_URL'),
],
'google' => [
    'client_id' => env('GOOGLE_APP_ID'),
    'client_secret' => env('GOOGLE_APP_SECRET'),
    'redirect' => env('GOOGLE_APP_CALLBACK_URL'),
],
```
5. Create **UserSocial** 
```php artisan make:model UserSocial -m```
6. Edit file **Migrate** 
```php
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->text('avatar_url')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
```
7. Replace all **UserSocial** by
```php
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSocial extends Model
{
    protected $fillable = ['user_id', 'provider_id', 'provider'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
```
8. Create **UserSocialController** 
```php php artisan make:controller UserSocialController```
9. Replace all **UserSocialController** by
```php
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserSocial;
use Socialite, Auth, Redirect, Session, URL;

class UserSocialController extends Controller
{
    public function redirectToProvider($provider)
    {
        if (!Session::has('pre_url')) {
            Session::put('pre_url', URL::previous());
        } else {
            if (URL::previous() != URL::to('login')) Session::put('pre_url', URL::previous());
        }
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser);
        return Redirect::to(Session::get('pre_url'));
    }

    public function findOrCreateUser($user, $provider)
    {
        $find = [
            'provider_id' => $user->id,
            'provider' => $provider,
        ];

        $account = UserSocial::where($find)->first();

        if ($account) {
            $account->user->avatar_url = $user->avatar;
            $account->user->save();
            return $account->user;
        } else {
            $account = new UserSocial([
                'provider_id' => $user->id,
                'provider' => $provider
            ]);

            $authUser = User::where('email', $user->email)->first();

            if (!$authUser) {
                $authUser = new User;
                $authUser->email = $user->email;
                $authUser->name = $user->name;
                $authUser->password = bcrypt($user->email);
                $authUser->avatar_url = $user->avatar;
                $authUser->save();
            }
            else{
                $authUser->avatar_url = $user->avatar;
                $authUser->save();
            }
            
            $account->user()->associate($authUser);
            $account->save();

            return $authUser;
        }
    }
}
```
10. Add into **routes/web.php** 
```php
Route::get('/auth/{provider}', 'UserSocialController@redirectToProvider')->name('');
Route::get('/auth/{provide}/callback', 'UserSocialController@handleProviderCallback')->name('');
```
11. Update file **.env**
```php
FACEBOOK_APP_ID = ???
FACEBOOK_APP_SECRET = ???
FACEBOOK_APP_CALLBACK_URL = /auth/facebook/callback

GOOGLE_APP_ID = ???
GOOGLE_APP_SECRET = ???
GOOGLE_APP_CALLBACK_URL = /auth/google/callback
```
---